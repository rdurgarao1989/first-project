def printTwoParts(arr, n) : 
   
    splitPo = findSplitPoint(arr, n) 
    
    if (splitPo == -1 or splitPo == n ) : 
        print ("Not Possible") 
        return
       
    for i in range(0, n) : 
        if(splitPo == i) : 
            print ("") 
        print (str(arr[i]) + ' ',end='') 
arr = [1,7,8,2]
n = len(arr) 
printTwoParts(arr, n) 
